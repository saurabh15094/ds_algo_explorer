import java.util.Arrays;

public class SortingOnArray {

    public static void main(String[] args) {
        int[] arr = {12, 4, 56, 3, 6, 990, 67};
        Arrays.sort(arr);
        for (int val : arr) {
            System.out.println(val);
        }
    }
}
