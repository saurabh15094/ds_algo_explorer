import java.sql.SQLOutput;
import java.util.Arrays;

public class LongestCommonSubsequence {

    public static void main(String[] args) {
        String a = "abcab";
        String b = "aecb";
        System.out.println(LongestCommonSubsequence(a, b));
        System.out.println(LongestCommonSubsequenceTopDown(a, b));
        System.out.println(lcsBottomUp(a, b));
    }

    static int lcsBottomUp(String a, String b) {
        int m = a.length();
        int n = b.length();
        int dp[][] = new int[m + 1][n + 1];

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (a.charAt(i - 1) == b.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
                System.out.print(dp[i][j] + " ");
            }
            System.out.println();
        }
        return dp[m][n];
    }


    static int LongestCommonSubsequence(String a, String b) {
        return LongestCommonSubsequenceUtil(a, b, a.length() - 1, b.length() - 1);
    }

    static int LongestCommonSubsequenceUtil(String a, String b, int i, int j) {
        if (i < 0 || j < 0) return 0;

        if (a.charAt(i) == b.charAt(j)) {
            return LongestCommonSubsequenceUtil(a, b, i - 1, j - 1) + 1;
        } else {
            return Math.max(LongestCommonSubsequenceUtil(a, b, i - 1, j), LongestCommonSubsequenceUtil(a, b, i, j - 1));
        }
    }


    static int LongestCommonSubsequenceTopDown(String a, String b) {
        int[][] dp = new int[a.length()][b.length()];
        for (int[] x : dp) {
            Arrays.fill(x, -1);
        }
        int ans = LongestCommonSubsequenceUtilTopDown(a, b, a.length() - 1, b.length() - 1, dp);
        for (int[] x : dp) {
            for (int v : x) {
                System.out.print(v + " ");
            }
            System.out.println();
        }
        return ans;
    }

    static int LongestCommonSubsequenceUtilTopDown(String a, String b, int i, int j, int[][] dp) {
        if (i < 0 || j < 0) return 0;

        if (a.charAt(i) == b.charAt(j)) {
            return dp[i][j] = LongestCommonSubsequenceUtilTopDown(a, b, i - 1, j - 1, dp) + 1;
        } else {
            return dp[i][j] = Math.max(LongestCommonSubsequenceUtilTopDown(a, b, i - 1, j, dp), LongestCommonSubsequenceUtilTopDown(a, b, i, j - 1, dp));
        }
    }
}
