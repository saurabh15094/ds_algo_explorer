public class ConvertS1ToS2WithMinInstionDeletion {

    public static void main(String[] args) {
        String a = "abcab";
        String b = "aecb";
        System.out.println(ConvertS1ToS2WithMinInsetionDeletion(a,b));
    }

    public static int ConvertS1ToS2WithMinInsetionDeletion(String s1, String s2){
        int lcs = LongestCommonSubsequence(s1 , s2);
        return s1.length()+s2.length()-2*lcs;

    }

    static int LongestCommonSubsequence(String a, String b) {
        return LongestCommonSubsequenceUtil(a, b, a.length() - 1, b.length() - 1);
    }

    static int LongestCommonSubsequenceUtil(String a, String b, int i, int j) {
        if (i < 0 || j < 0) return 0;

        if (a.charAt(i) == b.charAt(j)) {
            return LongestCommonSubsequenceUtil(a, b, i - 1, j - 1) + 1;
        } else {
            return Math.max(LongestCommonSubsequenceUtil(a, b, i - 1, j), LongestCommonSubsequenceUtil(a, b, i, j - 1));
        }
    }

}
