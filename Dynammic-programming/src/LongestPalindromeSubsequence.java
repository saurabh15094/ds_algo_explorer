public class LongestPalindromeSubsequence{
    public static void main(String[] args) {
        String s = "ASCFEFJPBHGJPI";
        System.out.println(LongestPalindromeSubsequence(s));
    }
    static int LongestPalindromeSubsequence(String s) {
        int n = s.length();
        char[] temp = new char[n];
        for(int i=n-1; i>=0; i--) {
            temp[n-1-i]=s.charAt(i);
        }
        String s2 = new String(temp);
        return LongestCommonSubsequence(s, s2);
    }

    static int LongestCommonSubsequence(String a, String b) {
        return LongestCommonSubsequenceUtil(a, b, a.length() - 1, b.length() - 1);
    }

    static int LongestCommonSubsequenceUtil(String a, String b, int i, int j) {
        if (i < 0 || j < 0) return 0;

        if (a.charAt(i) == b.charAt(j)) {
            return LongestCommonSubsequenceUtil(a, b, i - 1, j - 1) + 1;
        } else {
            return Math.max(LongestCommonSubsequenceUtil(a, b, i - 1, j), LongestCommonSubsequenceUtil(a, b, i, j - 1));
        }
    }

}

